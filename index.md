## Purpose of the bot
Be useful when it comes to interaction with Tableau server (either [Online][2]
or [On-site][1]) via [_Slack_][3].

## Available commands

#### list

**list** command helps you can explore the server objects (like _projects, workbooks, views_).

_Syntax_: list {type}

Where _type_ from this list: projects, workbooks, views, aliases, last, commands, cron

For example: _list projects_

Result:

№  | Result
------------- | -------------
#1  | default
#2  | Marketing
#3  | Finance
#4  | Operations
#5  | Customer Support


#### set

**set** command sets the current object of specified type (like _project, workbook_).

_Syntax_: set project|workbook {item_name}

Where _item_name_ is either name or id from the results of _list_ command.

Example:

_list projects_

Result:

№  | Result
------------- | -------------
#1  | default
#2  | Marketing
#3  | Finance
#4  | Operations
#5  | Customer Support

_set project Finance_ or _set project 3_

Result:

_Current project is set to "Finance"_


#### show

**show** command shows the current object that were set (like _project, workbook_).

_Syntax_: show project|workbook

Example: _show project_

Result:

_Current project is "Finance"_


#### clear

**clear** command resets the current object of specified type (like _project, workbook_).

_Syntax_: clear project|workbook

For example: _clear project_

Result:

_Current project was reset to default value"_


#### get

**get** command provides with a representation of the workbook or view with following file formats: PNG, PDF, CVS.

_Syntax_: get png|pdf|csv -w|v {item_name}\s\s
Where _item_name_ is the name from the results of _list {type}_ or alias.\s\s
Note: png is the default value for file format

Example:

Run: _set project Finance_

Result: _Current project is set to "Finance"_

Run: _list workbooks_

Result:

№  | Result
------------- | -------------
#1  | Finance overview
#2  | Monthly reporting
#3  | Spend report

Run: _get png -w 1_ or _get -w 1_ or _get -w Finance overview_

Result: _PNG version of the report._


#### alias+

**alias+** command creates an alias for an object (like _project, workbook_).
_Syntax_: alias+ -id {item_id} -a {alias_name}
Where _-id_ is an id from the results of _list {type}_ and _alias_name_ any name for an alias.
Note: name could be almost anything, should be unique (you can't create few aliases with the same name).
And you can't create an alias on another alias :)

Example:
After the execution of this command _set project Finance_ run _list workbooks_

Result:

№  | Result
------------- | -------------
#1  | Finance overview
#2  | Monthly reporting
#3  | Spend report

Then create an alias: _alias+ -id 2 -a M_rep_

Result: _Alias "M_rep" for workbook "Monthly reporting" was created successfully!_


#### alias-

**alias-** command removes an alias from user's aliases.
_Syntax_: alias- -a {alias_name}

For example based on the previous results: _set project Finance_ or _set project 3_

Run: _alias- -a M_re_

Result: _Alias "M_rep" for workbook was deleted successfully!_


#### cron+

**cron+** command can schedule your task.
_Syntax_: cron+ pdf|png|csv {item} ' {schedule_expression} '
Where _item_ it's either related to the result of previous _list workbooks|views|aliases_ or _list last_ if last time you listed workbooks/views/aliases.
And _schedule_expression_ are expression for cron, [Cron tutorial][4].
_Note: there is no seconds so expression starts with minutes._

Example:
Run: _cron+ png M_rep ' 0 0/3 * * '_\s\s
Will be scheduled every 3 hour starting from 0 at 0 minute

Result: _Schedule 'Schedule #123' for workbook 'Monthly reporting' was created successfully!_


#### cron-

**cron-** removes a schedule from your schedules list.
_Syntax_: cron- {item_id}
_Note: before that you need to list your schedules: list cron
When you list schedules they are shown by channel, to see for all channels run with -all param_

Example:
Run: _list cron_

№  |  Result  | Definition
------------- | -------------
#1  | Schedule #123 | "Monthly Reporting" as PNG in channel#default at ' 0 0/3 * * '
#2  | Schedule #456 | "Spend report" as PDF in channel#direct as ' 0 9 * * '

Run: _cron- 1_

Result: _Schedule "Schedule #123" for workbook "Monthly reporting" was deleted successfully!_


#### man

**man** command provides short info about the command specified (like _list, set_).
_Syntax_: man {command}

Example:

Run: _man list_

Result: _**list** command returns a list of objects available in the category_



[1]: http://www.tableau.com/products/server		  "On-site"
[2]: http://www.tableau.com/products/cloud-bi	  "Online"
[3]: https://slack.com							  "Slack"
[4]: http://www.quartz-scheduler.org/documentation/quartz-2.2.x/tutorials/tutorial-lesson-06.html "Cron"
